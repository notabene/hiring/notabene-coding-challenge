import * as path from 'path';
import { Connection, getConnectionManager } from 'typeorm';
import { Claim } from '../entities/Claim';

export async function connect(name = 'default'): Promise<Connection> {
  const connectionManager = getConnectionManager();

  if (connectionManager.has(name)) {
    const connection = connectionManager.get(name);

    if (!connection.isConnected) {
      await connection.connect();
    }

    return connection;
  }

  return await connectionManager
    .create({
      name,
      type: 'sqlite',
      database: path.join(process.cwd(), 'data/transactions.sqlite'),
      entities: [Claim],
    })
    .connect();
}
