import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class Claim {
  @PrimaryColumn()
  id: string;

  @Column()
  context: string;

  @Column()
  type: string;

  @Column()
  issuer: string;

  @Column()
  issuanceDate: string;

  @Column()
  credentialSubject: string;
}
