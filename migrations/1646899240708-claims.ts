import { MigrationInterface, QueryRunner } from 'typeorm';

export class claims1646899240708 implements MigrationInterface {
  name = 'claims1646899240708';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "claim" ("id" varchar PRIMARY KEY NOT NULL, "context" varchar NOT NULL, "type" varchar NOT NULL, "issuer" varchar NOT NULL, "issuanceDate" varchar NOT NULL, "credentialSubject" varchar NOT NULL)`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "claim"`);
  }
}
