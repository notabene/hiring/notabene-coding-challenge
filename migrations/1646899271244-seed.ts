import { MigrationInterface, QueryRunner } from 'typeorm';

export class seed1646899271244 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `INSERT INTO claim ("id","context","type","issuer","issuanceDate","credentialSubject")
          VALUES ('0892f680-6aeb-11eb-9bcf-f10d8993fde7', 'https://www.w3.org/2018/credentials/v1,https://www.w3.org/2018/credentials/examples/v1', 'VerifiableCredential,TransactionAction', 'did:example:76e12ec712ebc6f1c221ebfeb1f', '2022-03-10T09:00:00.000Z', '{"id":"36459542-af2d-4107-b979-645599b149c0","from":"9dbd151732fdbf6825a37109af298d3345ff62b4dca29073ba9b2cd5ed84c410","to":"3525b010f83fe053a7c4eb0d6064c2e1f7c00b68d0caa9d3b919be97d6b08ac7","asset":"BTC","amount":"0.0001","status":"NEW"}')`,
    );
    await queryRunner.query(
      `INSERT INTO claim ("id","context","type","issuer","issuanceDate","credentialSubject")
          VALUES ('8c60f79e-f395-4524-a237-18f2018d922a', 'https://www.w3.org/2018/credentials/v1,https://www.w3.org/2018/credentials/examples/v1', 'VerifiableCredential,TransactionAction', 'did:example:76e12ec712ebc6f1c221ebfeb1f', '2022-03-10T09:00:01.000Z', '{"id":"36459542-af2d-4107-b979-645599b149c0","status":"SENT"}')`,
    );
    await queryRunner.query(
      `INSERT INTO claim ("id","context","type","issuer","issuanceDate","credentialSubject")
          VALUES ('28e60d0a-2976-462a-87c7-9d24df1975fb', 'https://www.w3.org/2018/credentials/v1,https://www.w3.org/2018/credentials/examples/v1', 'VerifiableCredential,TransactionAction', 'did:example:76e12ec712ebc6f1c221ebfeb1f', '2022-03-10T10:00:00.000Z', '{"id":"b6532860-283d-4d0b-8349-ef2b008c7231","from":"9dbd151732fdbf6825a37109af298d3345ff62b4dca29073ba9b2cd5ed84c410","to":"3525b010f83fe053a7c4eb0d6064c2e1f7c00b68d0caa9d3b919be97d6b08ac7","asset":"BTC","amount":"0.0006","status":"NEW"}')`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.clearTable('claim');
  }
}
