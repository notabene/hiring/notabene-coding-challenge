# Senior Software Engineer

## Introduction

At Notabene, we use [Verifiable Credentials](https://www.w3.org/TR/vc-data-model/) as a way to express and share information about different entities such as VASPs, users and transactions.

## Stack

This project is a [Next.JS](https://nextjs.org/) application. It can provide API endpoints as well as a SSR frontend. The database is SQLite and [TypeORM](https://typeorm.io/) is used to provide an easy way to query for data in the database.

There are some migrations in the `/migrations` folder that should give you a hint to how the database is structured and what data is available for you to query.

There is a convenience method `connect` exported from `utils/db` that should help you connect to the database i.e.

```
import { connect } from 'utils/db';

...

const connection = await connect();
```

## Task 1: Transactions API

Your first task is to extend the service provided with an endpoint that returns information about transactions that are the subject of several claims in the database.

Here’s an example of what a simple claim looks like:

```json
{
  "id": "0892f680-6aeb-11eb-9bcf-f10d8993fde7",
  "context": "https://www.w3.org/2018/credentials/v1,https://www.w3.org/2018/credentials/examples/v1",
  "type": "VerifiableCredential,TransactionAction",
  "issuer": "did:example:76e12ec712ebc6f1c221ebfeb1f",
  "issuanceDate": "2022-03-10T09:00:00.000Z",
  "credentialSubject": "{\"id\":\"36459542-af2d-4107-b979-645599b149c0\",\"from\":\"9dbd151732fdbf6825a37109af298d3345ff62b4dca29073ba9b2cd5ed84c410\",\"to\":\"3525b010f83fe053a7c4eb0d6064c2e1f7c00b68d0caa9d3b919be97d6b08ac7\",\"asset\":\"BTC\",\"amount\":\"0.0001\",\"status\":\"NEW\"}"
}
```

An example of a transaction entity is as follows:

```json
{
  "id": "36459542-af2d-4107-b979-645599b149c0",
  "from": "9dbd151732fdbf6825a37109af298d3345ff62b4dca29073ba9b2cd5ed84c410",
  "to": "3525b010f83fe053a7c4eb0d6064c2e1f7c00b68d0caa9d3b919be97d6b08ac7",
  "asset": "BTC",
  "amount": "0.0001",
  "status": "NEW"
}
```

Your task will be to create an endpoint that iterates over the claims in the database and returns a list of transactions entities.

### Evaluation Criteria

- [ ] API endpoint is available at `GET http://localhost:3000/api/transactions`
- [ ] API returns an array of transaction entities

## Task 2: Transactions List Page

Your second task is to create a simple transactions page that fetches the transactions from `GET /api/transactions` and shows them on the frontend.

### Evaluation Criteria

- [ ] Page can be accessed at `http://localhost:3000/transactions`
- [ ] Page shows info about **2** transactions

## Getting Started

- Clone down the repository (You will be given access during the coding challenge).
- In the root of the project, run the following commands:
  - `yarn` to install dependencies
  - `yarn db:migrate` to run migrations and seed the DB
  - `yarn dev` to start the service
